import telebot
import requests
import json
bot=telebot.TeleBot('5927290873:AAEjOQCb8esiJdRIcHZmLQq3n4FgI3VxtQ0')
API='13c64e9a455219a4953a7b2588f23e31'
#типи погоди та відповідні посилання на картинки
weather_icons = {
    'Clear': 'sun.png',
    'Clouds': 'cloud.jpg',
    'Rain': 'rain.png',
    'Snow': 'snow.jpg',
    
}
#реакція на команду старт
@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id, 'Привіт! Напиши назву міста, щоб дізнатись інформацію про погоду в ньому')
#реакція на команду help
@bot.message_handler(commands=['help'])
def help(message):
    help_text = "Функції які присутні в боті:\n"
    help_text += "/start - початок роботи з ботом\n"
    help_text += "/help - відображення списку функцій\n"
    help_text += "Отримайте погоду, просто увівши назву міста"
    bot.send_message(message.chat.id, help_text)
#Аналіз введеного тексту та отримання даних про погоду 
@bot.message_handler(content_types=['text'])
def get_weather(message):
    city = message.text.strip().lower()
    req = requests.get(f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={API}&units=metric')
    
    if req.status_code == 200:
        data = json.loads(req.text)
        
        if 'weather' in data:
            weather = data['weather'][0]
            weather_main = weather['main']
            weather_description = weather['description']

            if weather_main in weather_icons:
                weather_icon_url = weather_icons[weather_main]
                bot.send_photo(message.chat.id, open(weather_icon_url, 'rb'))
                
                bot.reply_to(message, f'Зараз температура в місті становить {data["main"]["temp"]}°C\n'
                                  f'Відчувається як {data["main"]["feels_like"]}°C\n'
                                  f'Максимальна очікувана температура {data["main"]["temp_max"]}°C\n'
                                  f'Мінімальна очікувана температура {data["main"]["temp_min"]}°C\n'
                                  f'Вологість {data["main"]["humidity"]}%\n'
                                  f'Швидкість вітру {data["wind"]["speed"]} м/с\n')
        else:
            bot.reply_to(message, f'Зараз температура в місті становить {data["main"]["temp"]}°C\n'
                                  f'Відчувається як {data["main"]["feels_like"]}°C\n'
                                  f'Максимальна очікувана температура {data["main"]["temp_max"]}°C\n'
                                  f'Мінімальна очікувана температура {data["main"]["temp_min"]}°C\n'
                                  f'Вологість {data["main"]["humidity"]}%\n'
                                  f'Швидкість вітру {data["wind"]["speed"]} м/с\n')
    else:
        bot.send_message(message.chat.id, "Введена некоректна інформація")

bot.polling(none_stop=True)